package edu.sjsu.android.gradebook;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import edu.sjsu.android.dataprovider.StudentsProvider;
import edu.sjsu.android.gradebook.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private Uri uri = StudentsProvider.CONTENT_URI;
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
    }

    public void addStudent(View view) {
        ContentValues values = new ContentValues();
        values.put("name", binding.studentName.getText().toString());
        values.put("grade", binding.studentGrade.getText().toString());
        // Toast message if successfully inserted
        if (getContentResolver().insert(uri, values) != null)
            Toast.makeText(this, "Student Added", Toast.LENGTH_SHORT).show();
    }

    public void getAllStudents(View view) {
        // Sort by student name
        try (Cursor c = getContentResolver().
                query(uri, null, null, null, "_id")) {
            if (c.moveToFirst()) {
                String result = "Tony Xie's Gradebook: \n";
                do {
                    for (int i = 0; i < c.getColumnCount(); i++) {
                        result = result.concat
                                (c.getString(i) + "\t");
                    }
                    result = result.concat("\n");
                } while (c.moveToNext());
                binding.result.setText(result);
            }
        }
    }
}